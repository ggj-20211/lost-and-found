# Lost and Found

## Table Of Contents

- [About the project](#about-the-project)
    - [Summary](#summary)
    - [Build With](#build-with)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Contributing](#contributing)
    - [Process](#process)
    - [Team](#team)
- [Contact](#contact)
- [License](#license)

## About the project

### Summary

Video game produced for [Global Game Jam 2021](https://globalgamejam.org/2021)

### Build With

- [Unity](https://unity.com/) is a real-time engine that helps creators to make cross-platform video games

## Getting started

### Prerequisites

- Download and install [Unity](https://unity3d.com/get-unity/download/)

### Installation

1. Clone the repo

```bash
git clone git@gitlab.com:ggj-20211/lost-and-found.git
```

2. Open the project with Unity

## Contributing

### Process

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. [Fork the Project][fork-url]
2. Create your Feature Branch (`git checkout -b feature/amazing-feature`)
3. Commit your Changes (`git commit -m "add some amazing feature"`)
4. Push to the Branch (`git push origin feature/amazing-feature`)
5. Open a Pull Request

### Team

- [Marc Gavanier][linkedin-url]
- [Add your name here...](#contributing)

## Contact

You can reach me at [marc.gavanier@gmail.com](marc.gavanier@gmail.com).

## License

Released under the terms of the `MIT` license. See [LICENSE](/LICENSE) for the full details.

[fork-url]: https://gitlab.com/ggj-20211/lost-and-found/-/forks/new
[linkedin-url]: https://www.linkedin.com/in/marc-gavanier
