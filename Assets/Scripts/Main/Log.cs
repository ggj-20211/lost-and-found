﻿using UnityEngine;

namespace Main
{
    public class Log : MonoBehaviour
    {
        public string Message;

        void Start()
        {
            Debug.Log(Message);
        }
    }
}
