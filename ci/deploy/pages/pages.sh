set -e
set -x

mkdir -p public
rm -rf public/$CI_COMMIT_REF_SLUG
cp -r Builds/WebGL/$BUILD_NAME/ public/$CI_COMMIT_REF_SLUG/
echo "$CI_PAGES_URL/$CI_COMMIT_REF_SLUG/"
